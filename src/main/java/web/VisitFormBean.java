package main.java.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import main.java.domain.objects.Visit;
import main.java.repositories.IRepositoryCatalog;

@SessionScoped
@Transactional
@Named("visitBean")
public class VisitFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Visit> visits = new ListDataModel<Visit>();

	private Visit visit = new Visit();

	@Inject
	private IRepositoryCatalog catalog;

	public Visit getVisit() {
		return visit;
	}

	public void setVisit(Visit visit) {
		this.visit = visit;
	}

	public String saveVisit() {
		catalog.getVisits().add(visit);
		visit = new Visit();
		return "showVisits";
	}

	public ListDataModel<Visit> getVisits() {

		visits.setWrappedData(catalog.getVisits().getAll());
		return visits;
	}

	public String deleteVisit() {
		Visit visit = visits.getRowData();
		Visit visitToDelete = catalog.getVisits().get(visit.getId());
		catalog.getVisits().delete(visitToDelete);
		return "showVisit";
	}

	public String editVisit() {
		visit = visits.getRowData();

		return "editVisit";
	}
	
	public String changeAll() {

		Visit v = catalog.getVisits().get(visit.getId());
		v.setVisittimefrom(visit.getVisittimefrom());
		v.setVisittimeto(visit.getVisittimeto());
		v.setVisitdate(visit.getVisitdate());
		return "showVisits";
	}

}