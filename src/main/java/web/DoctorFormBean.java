package main.java.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import main.java.domain.objects.Doctor;
import main.java.repositories.IRepositoryCatalog;

@SessionScoped
@Transactional
@Named("doctorBean")
public class DoctorFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Doctor> doctors = new ListDataModel<Doctor>();

	private Doctor doctor = new Doctor();

	@Inject
	private IRepositoryCatalog catalog;

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String saveDoctor() {
		catalog.getDoctors().add(doctor);
		doctor = new Doctor();
		return "showDocotrs";
	}

	public ListDataModel<Doctor> getDoctors() {

		doctors.setWrappedData(catalog.getDoctors().getAll());
		return doctors;
	}

	public String deleteDoctor() {
		Doctor doctor = doctors.getRowData();
		Doctor doctorToDelete = catalog.getDoctors().get(doctor.getId());
		catalog.getDoctors().delete(doctorToDelete);
		return "showDoctors";
	}

	public String editDoctor() {
		doctor = doctors.getRowData();

		return "editDoctor";
	}
	
	public String changeAll() {

		Doctor d = catalog.getDoctors().get(doctor.getId());
		d.setFirstname(doctor.getFirstname());
		d.setSurname(doctor.getSurname());
		d.setSpecialization(doctor.getSpecialization());
		d.setWorkhoursfrom(doctor.getWorkhoursfrom());
		d.setWorkhoursto(doctor.getWorkhoursto());	
		return "showDoctors";
	}

}