package main.java.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import main.java.domain.objects.Facility;
import main.java.repositories.IRepositoryCatalog;

@SessionScoped
@Transactional
@Named("facilityBean")
public class FacilityFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Facility> facilities = new ListDataModel<Facility>();

	private Facility facility = new Facility();

	@Inject
	private IRepositoryCatalog catalog;

	public Facility getFacility() {
		return facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

	public String saveFacility() {
		catalog.getFacilities().add(facility);
		facility = new Facility();
		return "showFacilities";
	}

	public ListDataModel<Facility> getFacilities() {

		facilities.setWrappedData(catalog.getFacilities().getAll());
		return facilities;
	}

	public String deleteFacility() {
		Facility facility = facilities.getRowData();
		Facility facilityToDelete = catalog.getFacilities().get(facility.getId());
		catalog.getFacilities().delete(facilityToDelete);
		return "showFacilities";
	}

	public String editFacility() {
		facility = facilities.getRowData();

		return "editFacility";
	}
	
	public String changeAll() {

		Facility f = catalog.getFacilities().get(facility.getId());
		f.setName(facility.getName());
		f.setAddress(facility.getAddress());

		return "showFacilities";
	}

}