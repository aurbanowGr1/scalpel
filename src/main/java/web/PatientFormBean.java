package main.java.web;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import main.java.domain.objects.Patient;
import main.java.repositories.IRepositoryCatalog;

@SessionScoped
@Transactional
@Named("patientBean")
public class PatientFormBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ListDataModel<Patient> patients = new ListDataModel<Patient>();

	private Patient patient = new Patient();

	@Inject
	private IRepositoryCatalog catalog;

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String savePatient() {
		catalog.getPatients().add(patient);
		patient = new Patient();
		return "showPatients";
	}

	public ListDataModel<Patient> getPatients() {

		patients.setWrappedData(catalog.getPatients().getAll());
		return patients;
	}

	public String deletePatient() {
		Patient patient = patients.getRowData();
		Patient patientToDelete = catalog.getPatients().get(patient.getId());
		catalog.getPatients().delete(patientToDelete);
		return "showPatient";
	}

	public String editPatient() {
		patient = patients.getRowData();

		return "editPatient";
	}
	
	public String changeAll() {

		Patient p = catalog.getPatients().get(patient.getId());
		p.setFirstname(patient.getFirstname());
		p.setSurname(patient.getSurname());
		p.setAddress(patient.getAddress());
		p.setPesel(patient.getPesel());
		return "showPatients";
	}

}