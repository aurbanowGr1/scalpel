package main.java.domain.objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


@Entity
@NamedQuery(name = "visit.all", query = "SELECT v from Visit v")
public class Visit extends EntityBase{	
	private int visittimefrom;
	private int visittimeto;
	private String  visitdate;
	
	@ManyToOne
	private Doctor doctor;
	
	public Doctor getDoctor(){
		return doctor;
	}
	
	public String getVisitdate() {
		return visitdate;
	}
	public void setVisitdate(String visitdate) {
		this.visitdate = visitdate;
	}
	public int getVisittimefrom() {
		return visittimefrom;
	}
	public void setVisittimefrom(int visittimefrom) {
		this.visittimefrom = visittimefrom;
	}
	public int getVisittimeto() {
		return visittimeto;
	}
	public void setVisittimeto(int visittimeto) {
		this.visittimeto = visittimeto;
	}
}