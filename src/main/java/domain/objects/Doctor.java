package main.java.domain.objects;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQuery(name = "doctor.all", query = "SELECT d FROM Doctor d")
public class Doctor extends EntityBase{
	public Doctor() {
			this.facilities = new HashSet<Facility>();
			this.visits = new HashSet<Visit>();
	}
	
	private String firstname;
	private String surname;
	private String specialization;
	private int workhoursfrom;
	private int workhoursto;
	
	@OneToMany
	private Facility facility;
	@OneToMany(mappedBy = "doctor", cascade = CascadeType.PERSIST)
	private Set<Facility> facilities;
	@OneToMany(mappedBy = "doctor", cascade = CascadeType.PERSIST)
	private Set<Visit> visits;
	
	public Facility getFacility() {
		return facility;
	}
	public void setFacility(Facility facility) {
		this.facility = facility;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSpecialization() {
		return specialization;
	}
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public int getWorkhoursto() {
		return workhoursto;
	}
	public void setWorkhoursto(int workhoursto) {
		this.workhoursto = workhoursto;
	}

	public int getWorkhoursfrom() {
		return workhoursfrom;
	}
	public void setWorkhoursfrom(int workhoursfrom) {
		this.workhoursfrom = workhoursfrom;
	}
	public Set<Facility> getFacilities(){
		return facilities;
	}
	public void setFacilities(Set<Facility> facilities){
		this.facilities = facilities;
	}
	public Set<Visit> getVisits(){
		return visits;
	}
	public void setVisits(Set<Visit> visits){
		this.visits = visits;
	}
	
}
