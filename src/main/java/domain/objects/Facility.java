package main.java.domain.objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


@Entity
@NamedQuery(name = "facility.all", query = "SELECT f from Facility f")
public class Facility extends EntityBase{
	private String name;
	private String address;
	
	@ManyToOne
	private Doctor doctor;
	
	public Doctor getDoctor(){
		return doctor;
	}
	
	public void setDoctor(Doctor doctor){
		this.doctor = doctor;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
