package main.java.domain.objects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQuery(name="patient.all", query="Select p from Patient p")
public class Patient extends EntityBase{
	private String firstname;
	private String surname;
	private String address;
	private String pesel;
	
	@ManyToMany(mappedBy="patient", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	private List<Facility> facilities;
	
	@ManyToMany(mappedBy="patient", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	private List<Doctor> doctors;
	
	@OneToOne(mappedBy="patient", cascade=CascadeType.PERSIST, fetch=FetchType.LAZY)
	private User user;
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
}
