package main.java.domain.objects;

public enum EntityState {
	New, Changed, UnChanged, Deleted
}
