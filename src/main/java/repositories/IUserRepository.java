package main.java.repositories;

import java.util.List;

import main.java.domain.objects.User;
import main.java.domain.objects.Visit;


public interface IUserRepository extends IRepository<User>{
	public List<User> getUserByFirstname (String firstname);
	public List<User> getUserBySurname (String surname);
	public List<User> getUserByVisit (Visit visit);

}
