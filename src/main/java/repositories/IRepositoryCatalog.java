package main.java.repositories;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.domain.objects.Patient;
import main.java.domain.objects.User;
import main.java.domain.objects.Visit;

public interface IRepositoryCatalog {
	public IRepository<Doctor> getDoctors();
	public IRepository<Facility> getFacilities();
	public IRepository<Visit> getVisits();
	public IRepository<User> getUsers();
	public IRepository<Patient> getPatients();
}
