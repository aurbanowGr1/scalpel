package main.java.repositories.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.domain.objects.User;
import main.java.domain.objects.Visit;
import main.java.repositories.IRepository;
import main.java.repositories.IRepositoryCatalog;

@Stateless
public class RepositoryCatalog implements IRepositoryCatalog{
	
	@Inject
	private EntityManager em;
	
	@Override
	public IRepository<Doctor> getDoctors(){
		return new JpaDoctorRepo(em);
	}

	@Override
	public IRepository<Facility> getFacilities(){
		return new JpaFacilityRepo(em);
	}
	
	@Override
	public IRepository<User> getUsers(){
		return new JpaUserRepo(em);
	}
	
	@Override
	public IRepository<Visit> getVisits(){
		return new JpaVisitRepo(em);
	}
}
