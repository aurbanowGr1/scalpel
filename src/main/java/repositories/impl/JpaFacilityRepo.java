package main.java.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.repositories.IRepository;
public class JpaFacilityRepo implements IRepository<Facility>{
	
	private EntityManager em;
	
	public JpaFacilityRepo(EntityManager em){
		this.em = em;
	}
	
	@Override
	public Facility get(int id){
		return em.find(Facility.class, id);
	}
	
	@Override
	public List<Facility> getAll(){
		return em.createNamedQuery("facility.all", Facility.class).getResultList();
	}

	@Override
	public void add(Facility entity){
		em.persist(entity);
	}
	
	@Override
	public void delete(Facility entity){
		em.remove(entity);
	}
	
	@Override
	public void update(Facility entity){
		
	}
}
