package main.java.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import main.java.domain.objects.Visit;
import main.java.repositories.IRepository;
public class JpaVisitRepo implements IRepository<Visit>{

private EntityManager em;
	
	public JpaVisitRepo(EntityManager em){
		this.em = em;
	}
	
	@Override
	public Visit get(int id){
		return em.find(Visit.class, id);
	}
	
	@Override
	public List<Visit> getAll(){
		return em.createNamedQuery("visit.all", Visit.class).getResultList();
	}

	@Override
	public void add(Visit entity){
		em.persist(entity);
	}
	
	@Override
	public void delete(Visit entity){
		em.remove(entity);
	}
	
	@Override
	public void update(Visit entity){
		
	}
}
