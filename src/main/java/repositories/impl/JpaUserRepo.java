package main.java.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import main.java.domain.objects.User;
import main.java.repositories.IRepository;

public class JpaUserRepo implements IRepository<User>{

	private EntityManager em;
	
	public JpaUserRepo(EntityManager em){
		this.em = em;
	}
	
	@Override
	public User get(int id){
		return em.find(User.class, id);
	}
	
	@Override
	public List<User> getAll(){
		return em.createNamedQuery("user.all", User.class).getResultList();
	}

	@Override
	public void add(User entity){
		em.persist(entity);
	}
	
	@Override
	public void delete(User entity){
		em.remove(entity);
	}
	
	@Override
	public void update(User entity){
		
	}
}
