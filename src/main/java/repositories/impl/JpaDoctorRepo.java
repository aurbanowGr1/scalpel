package main.java.repositories.impl;

import java.util.List;

import javax.persistence.EntityManager;

import main.java.domain.objects.Doctor;
import main.java.repositories.IRepository;
public class JpaDoctorRepo implements IRepository<Doctor>{
	
	private EntityManager em;
	
	public JpaDoctorRepo(EntityManager em){
		this.em = em;
	}
	
	@Override
	public Doctor get(int id){
		return em.find(Doctor.class, id);
	}
	
	@Override
	public List<Doctor> getAll(){
		return em.createNamedQuery("doctor.all", Doctor.class).getResultList();
	}

	@Override
	public void add(Doctor entity){
		em.persist(entity);
	}
	
	@Override
	public void delete(Doctor entity){
		em.remove(entity);
	}
	
	@Override
	public void update(Doctor entity){
		
	}
}
