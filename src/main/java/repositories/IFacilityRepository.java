package main.java.repositories;

import java.util.List;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.domain.objects.Visit;

public interface IFacilityRepository extends IRepository<Facility>{
	public List<Facility> getFacilityByName (String name);
	public List<Facility> getFacilityByAddress (String address);
	public List<Facility> getFacilityByDoctor (Doctor doctor);
	public List<Facility> getFacilityByVisit (Visit visit);
}
