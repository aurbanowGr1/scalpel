package main.java.repositories;

import java.util.List;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.domain.objects.User;
import main.java.domain.objects.Visit;

public interface IVisitRepository extends IRepository<Visit>{
	public List<Visit> getVisitByDoctor (Doctor doctor);
	public List<Visit> getVisitByUser (User user);
	public List<Visit> getVisitByFacility (Facility facility);
}
