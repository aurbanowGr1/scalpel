package main.java.repositories;

import java.util.List;

import main.java.domain.objects.Doctor;
import main.java.domain.objects.Facility;
import main.java.domain.objects.Visit;

public interface IDoctorRepository extends IRepository<Doctor>{
	public Doctor getDoctorByFirstname (String firstname);
	public Doctor getDoctorBySurname (String surname);
	public Doctor getDoctorByFacility (String facility);
	public Doctor getDoctorByVisit (Visit visit);
	public List<Doctor> getDoctorsByFacilities (Facility facility);
}
