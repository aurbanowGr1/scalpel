package main.java.repositories;

import java.util.List;

import main.java.domain.objects.EntityBase;

public interface IRepository<TEntity extends EntityBase>
{
	public TEntity get(int id);
	public List<TEntity> getAll();
	public void add(TEntity entity);
	public void delete(TEntity entity);
	public void update(TEntity entity);
}